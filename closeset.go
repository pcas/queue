// Closeset is a set of io.Closers with callback.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queue

import (
	"container/list"
	"io"
	"sync"
)

// closeSet maintains a set of io.Closers.
type closeSet struct {
	m sync.Mutex // Mutex controlling access
	l list.List  // The list of io.Closers in the set
}

/////////////////////////////////////////////////////////////////////////
// closeSet functions
/////////////////////////////////////////////////////////////////////////

// Include includes the io.Closer x in the set. Returns a function to remove x from the set.
func (s *closeSet) Include(x io.Closer) func() {
	// Add x to the list as a new element
	s.m.Lock()
	defer s.m.Unlock()
	e := s.l.PushBack(x)
	// Return a function removing the element from the list
	return func() {
		s.m.Lock()
		defer s.m.Unlock()
		s.l.Remove(e)
	}
}

// Close calls Close on all members currently in the set.
func (s *closeSet) Close() (err error) {
	// Grab the first element from the list
	s.m.Lock()
	e := s.l.Front()
	s.m.Unlock()
	// We keep removing elements until the list is empty
	for e != nil {
		// Close the element
		if closeErr := e.Value.(io.Closer).Close(); closeErr != nil {
			err = closeErr
		}
		// Remove the element from the list and move on
		s.m.Lock()
		s.l.Remove(e)
		e = s.l.Front()
		s.m.Unlock()
	}
	return
}
