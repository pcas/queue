// Errors defines common errors.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package errors

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"runtime"
	"strconv"
)

// Code represents a common queue error.
type Code uint8

// The valid error codes
const (
	ConnectionClosed = Code(iota)
	ConnectionCloseError
	IllegalQueueName
	UnableToDeleteQueue
	UnknownQueue
	UnableToConnectToQueue
	QueueClosed
	QueueCloseError
	UnableToGetQueueLen
	UnableToGetMessage
	UnableToPutMessage
	NilMessage
	UnknownMessage
	AlreadyAcknowledged
	UnableToAcknowledgeMessage
	AlreadyRequeued
	UnableToRequeueMessage
	MessageFailed
)

// codeToString maps error codes to strings.
var codeToString = map[Code]string{
	ConnectionClosed:           "connection closed",
	ConnectionCloseError:       "error closing connection",
	UnableToDeleteQueue:        "error deleting queue",
	IllegalQueueName:           "illegal queue name",
	UnknownQueue:               "unknown queue",
	UnableToConnectToQueue:     "error connecting to queue",
	QueueClosed:                "queue closed",
	QueueCloseError:            "error closing queue",
	UnableToGetQueueLen:        "error getting queue length",
	UnableToGetMessage:         "error getting message",
	UnableToPutMessage:         "error putting message",
	NilMessage:                 "illegal nil message",
	UnknownMessage:             "unknown message",
	AlreadyAcknowledged:        "the message has already been acknowledged",
	UnableToAcknowledgeMessage: "error acknowledging message",
	AlreadyRequeued:            "the message has already been requeued",
	UnableToRequeueMessage:     "error requeuing message",
	MessageFailed:              "the message is in a failed state",
}

// Error is the interface satisfied by an error with a Code.
type Error interface {
	// Code returns the code associated with this error.
	Code() Code
	// Error returns a string description of the error.
	Error() string
}

// stack represents a stack of program counters.
type stack []uintptr

// qError describes a queue error.
type qError struct {
	code  Code  // The error code
	cause error // The cause (if any)
	*stack
}

/////////////////////////////////////////////////////////////////////////
// Code functions
/////////////////////////////////////////////////////////////////////////

// String returns a description of the error with error code c.
func (c Code) String() string {
	s, ok := codeToString[c]
	if !ok {
		s = "Unknown error (error code: " + strconv.Itoa(int(c)) + ")"
	}
	return s
}

// New returns a new error with error code c. The error satisfies the Error interface.
func (c Code) New() error {
	return &qError{
		code:  c,
		stack: callers(1),
	}
}

// Wrap returns a new error with error code c and cause e. The error satisfies the Error interface.
func (c Code) Wrap(e error) error {
	if err, ok := e.(Error); ok && err.Code() == c {
		return err
	}
	return &qError{
		code:  c,
		cause: e,
		stack: callers(1),
	}
}

/////////////////////////////////////////////////////////////////////////
// stack functions
/////////////////////////////////////////////////////////////////////////

// StackTrace returns the associated stack trace.
func (s *stack) StackTrace() errors.StackTrace {
	f := make([]errors.Frame, len(*s))
	for i := 0; i < len(f); i++ {
		f[i] = errors.Frame((*s)[i])
	}
	return f
}

// callers returns a new stack trace. The argument skip is the number of stack frames to ascend, with 0 identifying the calling function.
func callers(skip int) *stack {
	if skip < 0 {
		skip = 0
	}
	const depth = 32
	var pcs [depth]uintptr
	n := runtime.Callers(skip+2, pcs[:])
	var st stack = pcs[0:n]
	return &st
}

/////////////////////////////////////////////////////////////////////////
// qError functions
/////////////////////////////////////////////////////////////////////////

// Code returns the error code.
func (e *qError) Code() Code {
	return e.code
}

// Unwrap returns the underlying cause of the error, if known.
func (e *qError) Unwrap() error {
	return e.cause
}

// Is returns true if and only if target is an Error with the same error code.
func (e *qError) Is(target error) bool {
	f, ok := target.(Error)
	return ok && e.Code() == f.Code()
}

// Error returns a description of the error.
func (e *qError) Error() string {
	msg := e.Code().String()
	if cause := e.Unwrap(); cause != nil {
		msg += ": " + cause.Error()
	}
	return msg
}

// Format implements fmt.Formatter with support for the following verbs:
//
//	%s    print the error.
//	%v    see %s
//	%+v   extended format. The location of the error will be printed.
func (e *qError) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			if cause := e.Unwrap(); cause != nil {
				fmt.Fprintf(s, "%+v", cause)
			}
			e.StackTrace().Format(s, verb)
			io.WriteString(s, "\n"+e.Code().String()) // Ignore any error
			return
		}
		fallthrough
	case 's', 'q':
		io.WriteString(s, e.Error()) // Ignore any error
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ShouldWrap returns true if e satisfies all of the following:
//   - e is not nil;
//   - e is not equal to one of context.Canceled or context.DeadlineExceeded;
//   - e is does not satisfy the Error interface.
func ShouldWrap(e error) bool {
	if e == nil ||
		e == context.Canceled ||
		e == context.DeadlineExceeded {
		return false
	}
	_, ok := e.(Error)
	return !ok
}
