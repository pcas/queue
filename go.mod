module bitbucket.org/pcas/queue

go 1.18

require (
	bitbucket.org/pcas/logger v0.1.43
	bitbucket.org/pcas/metrics v0.1.35
	bitbucket.org/pcas/rabbitmq v0.0.1
	bitbucket.org/pcas/sslflag v0.0.16
	bitbucket.org/pcastools/address v0.1.4
	bitbucket.org/pcastools/cleanup v1.0.4
	bitbucket.org/pcastools/contextutil v1.0.3
	bitbucket.org/pcastools/flag v0.0.19
	bitbucket.org/pcastools/grpcutil v1.0.14
	bitbucket.org/pcastools/hash v1.0.5
	bitbucket.org/pcastools/listenutil v0.0.10
	bitbucket.org/pcastools/log v1.0.4
	bitbucket.org/pcastools/timeutil v0.1.3
	bitbucket.org/pcastools/ulid v0.1.6
	bitbucket.org/pcastools/version v0.0.5
	github.com/grpc-ecosystem/go-grpc-middleware v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.9.0
	google.golang.org/grpc v1.62.1
	google.golang.org/protobuf v1.33.0
)

require (
	bitbucket.org/pcastools/bytesbuffer v1.0.3 // indirect
	bitbucket.org/pcastools/compress v1.0.5 // indirect
	bitbucket.org/pcastools/convert v1.0.5 // indirect
	bitbucket.org/pcastools/gobutil v1.0.4 // indirect
	bitbucket.org/pcastools/pool v1.0.4 // indirect
	bitbucket.org/pcastools/stringsbuilder v1.0.3 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fatih/color v1.16.0 // indirect
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/klauspost/compress v1.17.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/petar/GoLLRB v0.0.0-20210522233825-ae3b015fd3e9 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rabbitmq/amqp091-go v1.9.0 // indirect
	github.com/shirou/gopsutil v3.21.11+incompatible // indirect
	github.com/tklauser/go-sysconf v0.3.13 // indirect
	github.com/tklauser/numcpus v0.7.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/term v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240308144416-29370a3891b7 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	nhooyr.io/websocket v1.8.10 // indirect
)
