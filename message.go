// Queue describes a message queue.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queue

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/queue/driver"
	"bitbucket.org/pcas/queue/errors"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"strconv"
	"sync"
	"time"
)

// Message represents a message in a queue.
type Message interface {
	metrics.Metricser
	log.Logger
	// AppendContent appends the message content to dst and returns the
	// extended buffer.
	AppendContent(dst []byte) []byte
	// Content returns (a copy of) the message content.
	Content() []byte
	// Acknowledge removes the message from its queue.
	Acknowledge(context.Context) error
	// Requeue requeues the message.
	Requeue(context.Context) error
}

// state represents a message state
type state int

// The possible state codes
const (
	active = state(iota)
	closing
	acknowledged
	requeued
	failed
)

// message implements the Message interface.
type message struct {
	d       driver.Interface  // The underlying driver
	id      driver.ID         // The ID of the message
	content []byte            // The content of the message
	closef  func()            // The close function
	lg      log.Interface     // The logger
	met     metrics.Interface // The metrics endpoint
	closedC <-chan struct{}   // Closed when the refresh worked exits
	m       sync.Mutex        // Mutex protecting the following
	s       state             // The current state of the message
	doneC   chan<- struct{}   // Close to shut down background refresh worker
}

// closeTimeout is the maximum amount of time allowed attempting to close a Message.
const closeTimeout = 5 * time.Second

// messageCloser wraps a Message linking Requeue to a close method.
type messageCloser struct {
	Message
}

////////////////////////////////////////////////////////////////////////////////
// Local functions
////////////////////////////////////////////////////////////////////////////////

// refreshDuration returns a duration by with a message with given deadline should be refreshed.
func refreshDuration(t time.Time) time.Duration {
	d := time.Until(t)
	if d >= 2*time.Minute {
		return d - time.Minute
	} else if d >= 2*time.Second {
		return d / 2
	} else if d >= time.Second {
		return time.Second
	}
	return d
}

// refreshWithID attempts to refresh the deadline for the message with given ID.
func refreshWithID(ctx context.Context, d driver.Interface, id driver.ID, deadline time.Time) (time.Time, error) {
	ctx, cancel := context.WithDeadline(ctx, deadline)
	defer cancel()
	return d.Refresh(ctx, id)
}

// refreshWorker starts a background go-routine refreshing the message with given ID, and initial given deadline, using the driver d. It is the caller's responsibility to close the returned doneC channel to shut-down the worker, otherwise resources may leak. The returned closedC channel will be closed when the worker exits.
func refreshWorker(d driver.Interface, id driver.ID, deadline time.Time) (doneC chan<- struct{}, closedC <-chan struct{}) {
	localDoneC, localClosedC := make(chan struct{}), make(chan struct{})
	go func(doneC <-chan struct{}, closedC chan<- struct{}) {
		// Defer closing the closedC channel on exit
		defer close(closedC)
		// Create a context that will fire when doneC is closed
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		defer contextutil.NewChannelLink(doneC, cancel).Stop()
		// Create the refresh timer
		t := time.NewTimer(refreshDuration(deadline))
		defer t.Stop()
		// Start refreshing the message
		for {
			select {
			case <-t.C:
				var err error
				deadline, err = refreshWithID(ctx, d, id, deadline)
				if err != nil {
					return
				}
				t.Reset(refreshDuration(deadline))
			case <-doneC:
				return
			}
		}
	}(localDoneC, localClosedC)
	return localDoneC, localClosedC
}

////////////////////////////////////////////////////////////////////////////////
// state functions
////////////////////////////////////////////////////////////////////////////////

// String returns a string description of the state.
func (s state) String() string {
	switch s {
	case closing:
		return "closing"
	case acknowledged:
		return "acknowledged"
	case requeued:
		return "requeued"
	case failed:
		return "failed"
	default:
		return "unknown (" + strconv.Itoa(int(s)) + ")"
	}
}

////////////////////////////////////////////////////////////////////////////////
// message functions
////////////////////////////////////////////////////////////////////////////////

// AppendContent appends the message content to dst and returns the extended buffer.
func (m *message) AppendContent(dst []byte) []byte {
	if m == nil || len(m.content) == 0 {
		return dst
	}
	return append(dst, m.content...)
}

// Content returns (a copy of) the message content.
func (m *message) Content() []byte {
	if m == nil || len(m.content) == 0 {
		return nil
	}
	b := make([]byte, len(m.content))
	copy(b, m.content)
	return b
}

// setState will attempt to place the message in state s. The only valid states for s are 'acknowledged', to acknowledge the message, or 'requeued', to requeue the message.
func (m *message) setState(ctx context.Context, s state) error {
	// Sanity check
	if s != acknowledged && s != requeued {
		return fmt.Errorf("attempting to transition to an invalid state: %s", s)
	}
	// First we check the current state, asking the refresh worker to exit if
	// necessary
	current := func() state {
		// Acquire a lock on the mutex
		m.m.Lock()
		defer m.m.Unlock()
		// If necessary, update our state to closing and ask the background
		// refresh worker to exit
		if m.s == active {
			m.s = closing
			close(m.doneC)
		}
		return m.s
	}()
	// Is it appropriate to change our state?
	switch current {
	case acknowledged:
		return errors.AlreadyAcknowledged.New()
	case requeued:
		return errors.AlreadyRequeued.New()
	case failed:
		return errors.MessageFailed.New()
	}
	// Wait for the refresh worker to exit
	<-m.closedC
	// Acquire a lock on the mutex
	m.m.Lock()
	defer m.m.Unlock()
	// Is there anything to do?
	switch m.s {
	case acknowledged:
		return errors.AlreadyAcknowledged.New()
	case requeued:
		return errors.AlreadyAcknowledged.New()
	case failed:
		return errors.MessageFailed.New()
	}
	// Update our state, ensuring we recover from any driver panics
	var err error
	switch s {
	case acknowledged:
		err = func() (err error) {
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in Acknowledge: %v", e)
				}
			}()
			err = m.d.Acknowledge(ctx, m.id)
			return
		}()
	case requeued:
		err = func() (err error) {
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in Requeue: %v", e)
				}
			}()
			err = m.d.Requeue(ctx, m.id)
			return
		}()
	}
	// Call the close function
	if m.closef != nil {
		m.closef()
	}
	// Handle any errors and return
	if err != nil {
		m.s = failed
	} else {
		m.s = s
	}
	return err
}

// Acknowledge removes the message from its queue.
func (m *message) Acknowledge(ctx context.Context) error {
	// Sanity check
	if m == nil || m.d == nil {
		return errors.NilMessage.New()
	}
	// Change the message state
	now := time.Now()
	err := m.setState(ctx, acknowledged)
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToAcknowledgeMessage.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(m.Metrics(), "Acknowledge", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		m.Log().Printf("Acknowledge succeeded in %s", dur)
	} else {
		m.Log().Printf("Acknowledge failed after %s: %v", dur, err)
	}
	return err
}

// Requeue requeues the message.
func (m *message) Requeue(ctx context.Context) error {
	// Sanity check
	if m == nil || m.d == nil {
		return errors.NilMessage.New()
	}
	// Change the message state
	now := time.Now()
	err := m.setState(ctx, requeued)
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToRequeueMessage.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(m.Metrics(), "Requeue", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		m.Log().Printf("Requeue succeeded in %s", dur)
	} else {
		m.Log().Printf("Requeue failed after %s: %v", dur, err)
	}
	return err
}

// Log returns the log for this message.
func (m *message) Log() log.Interface {
	if m == nil || m.lg == nil {
		return log.Discard
	}
	return m.lg
}

// Metrics returns the metrics endpoint for this message.
func (m *message) Metrics() metrics.Interface {
	if m == nil || m.met == nil {
		return metrics.Discard
	}
	return m.met
}

////////////////////////////////////////////////////////////////////////////////
// messageCloser functions
////////////////////////////////////////////////////////////////////////////////

// Close calls Requeue on the wrapped message.
func (m messageCloser) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), closeTimeout)
	defer cancel()
	err := m.Requeue(ctx)
	if err != nil {
		if e, ok := err.(errors.Error); ok {
			if e.Code() == errors.AlreadyAcknowledged || e.Code() == errors.AlreadyRequeued {
				err = nil
			}
		}
	}
	return err
}
