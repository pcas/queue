// Drivertest provides tests for implementations of driver.Interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package drivertest

import (
	"bitbucket.org/pcas/queue/driver"
	"context"
	"github.com/stretchr/testify/require"
	"math/rand"
	"strings"
	"testing"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialises the rand.Seed
func init() {
	rand.Seed(time.Now().Unix())
}

// wrap wraps a test function so that it can be passed to testing.T.Run.
func wrap(ctx context.Context, s driver.Interface, f func(context.Context, driver.Interface, *require.Assertions)) func(*testing.T) {
	return func(t *testing.T) {
		require := require.New(t)
		f(ctx, s, require)
	}
}

// wrapWithQueue wraps a test function so that it can be passed to testing.T.Run. This will create a random queue name and check that the corresponding queue is empty before running f. Once f completes, the queue will be deleted.
func wrapWithQueue(ctx context.Context, s driver.Interface, f func(context.Context, driver.Interface, string, *require.Assertions)) func(*testing.T) {
	return func(t *testing.T) {
		require := require.New(t)
		// Generate a random queue name
		name := "test-" + randomString(16)
		// Check that the queue is empty
		n, err := s.Len(ctx, name)
		require.NoError(err, "unable to check test queue \"%s\" is empty prior to running tests", name)
		require.Equal(0, n, "the test queue \"%s\" is not empty prior to running tests", name)
		// Defer deleting the queue
		defer func() {
			require.NoError(s.Delete(ctx, name), "unable to delete test queue \"%s\" after completing tests", name)
		}()
		// Run the tests
		f(ctx, s, name, require)
	}
}

// randomString returns a random string of length n.
func randomString(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

// sleep blocks for duration d.
func sleep(ctx context.Context, d time.Duration, require *require.Assertions) {
	t := time.NewTimer(d)
	select {
	case <-t.C:
	case <-ctx.Done():
		t.Stop()
		require.NoError(ctx.Err(), "test context fired whilst sleeping (sleep duration: %s)", d)
	}
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// testDriverName checks that the driver name isn't silly.
func testDriverName(ctx context.Context, s driver.Interface, require *require.Assertions) {
	driverName := s.DriverName()
	require.NotEqual(0, len(driverName), "the driver name must not be empty")
	t := strings.TrimSpace(driverName)
	require.Equal(driverName, t, "the driver name must not begin or end with white space")
}

// testEmpty tests that an empty queue behaves as expected.
func testEmpty(ctx context.Context, s driver.Interface, name string, require *require.Assertions) {
	// Deleting a non-existent queue should not error
	require.NoError(s.Delete(ctx, name), "deleting non-existent queue failed")
	// The length of a non-existent queue should be zero
	n, err := s.Len(ctx, name)
	require.NoError(err, "unable to recover length of non-existent queue")
	require.Equal(0, n, "non-existent queue should be of length 0")
	// Fetching from a non-existent queue should timeout
	ctx, cancel := context.WithTimeout(ctx, 100*time.Millisecond)
	defer cancel()
	_, _, _, err = s.Get(ctx, name)
	require.Error(err, "expected context timeout whilst fetching from non-existent queue")
	require.Equal(context.DeadlineExceeded, err, "expected context timeout whilst fetching from non-existent queue")
}

// testDelete tests deleting a non-empty queue.
func testDelete(ctx context.Context, s driver.Interface, name string, require *require.Assertions) {
	// Place some messages on to the queue
	const numMessages = 10
	messages := make([][]byte, numMessages)
	for i := range messages {
		content := []byte(randomString(5))
		require.NoError(s.Put(ctx, name, content), "unable to place test message %d/%d onto queue", i+1, numMessages)
		messages[i] = content
	}
	// At this point the queue should have length equal to numMessages. (Note
	// that this is only guaranteed to be true for all drivers because we have
	// not created any consumers.)
	n, err := s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(numMessages, n, "unexpected number of messages in queue")
	// Delete the queue
	require.NoError(s.Delete(ctx, name), "error deleting queue")
	// The length of the queue should be zero
	n, err = s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(0, n, "expected queue to be empty")
}

// testLen tests the length of a queue behaves as expected.
func testLen(ctx context.Context, s driver.Interface, name string, require *require.Assertions) {
	// Place some messages on to the queue
	const numMessages = 10
	for i := 0; i < numMessages; i++ {
		require.NoError(s.Put(ctx, name, []byte(randomString(5))), "unable to place test message %d/%d onto queue", i+1, numMessages)
	}
	// At this point the queue should have length equal to numMessages. (Note
	// that this is only guaranteed to be true for all drivers because we have
	// not created any consumers.)
	n, err := s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(numMessages, n, "unexpected number of messages in queue")
	// As we read in the messages, the length of the queue should drop
	for i := 0; i < numMessages; i++ {
		// Fetch a message
		id, _, _, err := s.Get(ctx, name)
		require.NoError(err, "error fetching message %d/%d", i+1, numMessages)
		// The queue should have length < numMessages - i
		n, err = s.Len(ctx, name)
		require.NoError(err, "error recovering length of queue")
		require.Less(n, numMessages-i, "queue contains too many messages")
		// Acknowledge the message
		require.NoError(s.Acknowledge(ctx, id), "unable to acknowledge message %d/%d (ID: %s)", i+1, numMessages, id)
		// The queue should still have length < numMessages - i
		n, err = s.Len(ctx, name)
		require.NoError(err, "error recovering length of queue")
		require.Less(n, numMessages-i, "queue contains too many messages")
	}
}

// testQueuing tests that if N messages are placed on a queue, those same N messages can be recovered from the queue.
func testQueuing(ctx context.Context, s driver.Interface, name string, require *require.Assertions) {
	// Place some messages on to the queue
	const numMessages = 1000
	messages := make([][]byte, numMessages)
	for i := range messages {
		content := []byte(randomString(20))
		require.NoError(s.Put(ctx, name, content), "unable to place test message %d/%d onto queue", i+1, numMessages)
		messages[i] = content
	}
	// At this point the queue should have length equal to numMessages. (Note
	// that this is only guaranteed to be true for all drivers because we have
	// not created any consumers.)
	n, err := s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(numMessages, n, "unexpected number of messages in queue")
	// Read the messages back
	received := make([][]byte, 0, numMessages)
	for i := 0; i < numMessages; i++ {
		id, c, _, err := s.Get(ctx, name)
		require.NoError(err, "error fetching message %d/%d", i+1, numMessages)
		require.NoError(s.Acknowledge(ctx, id), "error acknowledging message %d/%d (ID: %s)", i+1, numMessages, id)
		received = append(received, c)
	}
	require.ElementsMatch(messages, received, "the fetched messages do not agree with the submitted messages")
	// This should have emptied the queue
	n, err = s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(0, n, "expected empty queue")
	// Fetching from an empty queue should timeout
	ctx, cancel := context.WithTimeout(ctx, 100*time.Millisecond)
	defer cancel()
	_, _, _, err = s.Get(ctx, name)
	require.Error(err, "expected context timeout whilst fetching from empty queue")
	require.Equal(context.DeadlineExceeded, err, "expected context timeout whilst fetching from empty queue")
}

// testRequeuing tests that if N messages are placed on a queue, read, and requeued, then those same N messages can be recovered from the queue.
func testRequeuing(ctx context.Context, s driver.Interface, name string, require *require.Assertions) {
	// Place some messages on to the queue
	const numMessages = 100
	messages := make([][]byte, numMessages)
	for i := range messages {
		content := []byte(randomString(15))
		require.NoError(s.Put(ctx, name, content), "unable to place test message %d/%d onto queue", i+1, numMessages)
		messages[i] = content
	}
	// At this point the queue should have length equal to numMessages. (Note
	// that this is only guaranteed to be true for all drivers because we have
	// not created any consumers.)
	n, err := s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(numMessages, n, "unexpected number of messages in queue")
	// Read the messages back
	ids := make([]driver.ID, 0, numMessages)
	for i := 0; i < numMessages; i++ {
		id, _, _, err := s.Get(ctx, name)
		require.NoError(err, "error fetching message %d/%d", i+1, numMessages)
		ids = append(ids, id)
	}
	// This should have emptied the queue
	n, err = s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(0, n, "expected empty queue")
	// Pause before requeuing
	sleep(ctx, 100*time.Millisecond, require)
	// Requeue the messages
	for i, id := range ids {
		require.NoError(s.Requeue(ctx, id), "error requeuing message %d/%d (ID: %s)", i+1, numMessages, id)
	}
	// Now check that we can recover the original messages
	received := make([][]byte, 0, numMessages)
	for i := 0; i < numMessages; i++ {
		id, c, _, err := s.Get(ctx, name)
		require.NoError(err, "error fetching message %d/%d", i+1, numMessages)
		require.NoError(s.Acknowledge(ctx, id), "error acknowledging message %d/%d (ID: %s)", i+1, numMessages, id)
		received = append(received, c)
	}
	require.ElementsMatch(messages, received, "the fetched messages do not agree with the submitted messages")
	// This should have emptied the queue
	n, err = s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(0, n, "expected empty queue")
}

// testDeadline tests that a message is requeued when the deadline passes.
func testDeadline(ctx context.Context, s driver.Interface, name string, require *require.Assertions) {
	// Place a message on to the queue
	content := []byte(randomString(20))
	require.NoError(s.Put(ctx, name, content), "unable to place test message on queue")
	// Read the message back
	id, c, deadline, err := s.Get(ctx, name)
	require.NoError(err, "error fetching message")
	require.Equal(content, c, "fetched message does not agree with the submitted message (ID: %s)", id)
	// This should have emptied the queue
	n, err := s.Len(ctx, name)
	require.NoError(err, "error recovering queue length")
	require.Equal(0, n, "expected empty queue")
	// Wait until the message expires
	sleep(ctx, time.Until(deadline)+100*time.Millisecond, require)
	// The id of the message should now be invalid
	require.Error(s.Requeue(ctx, id), "expected an error requeuing message (deadline expired), but requeue succeeded (ID: %s)", id)
	// Grab the message back from the queue
	id, c, deadline, err = s.Get(ctx, name)
	require.NoError(err, "error fetching message")
	require.Equal(content, c, "fetched message does not agree with the submitted message (ID: %s)", id)
	// Wait for half of the expiry time
	sleep(ctx, time.Until(deadline)/2, require)
	// Refresh the deadline
	newDeadline, err := s.Refresh(ctx, id)
	require.NoError(err, "error refreshing message deadline (ID: %s)", id)
	// Check that the deadline was actually extended
	extra := newDeadline.Sub(deadline)
	require.Greater(extra, time.Duration(0), "refreshed message deadline (%s) is older than the original deadline (%s) (ID: %s)", newDeadline, deadline, id)
	// Wait until the old deadline has passed
	sleep(ctx, time.Until(deadline)+100*time.Millisecond, require)
	// Acknowledge the message
	require.NoError(s.Acknowledge(ctx, id), "error acknowledging message (ID: %s)", id)
	// Now the queue should be empty
	n, err = s.Len(ctx, name)
	require.NoError(err, "error recovering length of queue")
	require.Equal(0, n, "expected empty queue")
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Run runs the standard tests on the queue driver s.
func Run(ctx context.Context, s driver.Interface, t *testing.T) {
	t.Run("TestDriverName", wrap(ctx, s, testDriverName))
	t.Run("TestEmpty", wrapWithQueue(ctx, s, testEmpty))
	t.Run("TestDelete", wrapWithQueue(ctx, s, testDelete))
	t.Run("TestLen", wrapWithQueue(ctx, s, testLen))
	t.Run("TestQueuing", wrapWithQueue(ctx, s, testQueuing))
	t.Run("TestRequeuing", wrapWithQueue(ctx, s, testRequeuing))
	t.Run("TestDeadline", wrapWithQueue(ctx, s, testDeadline))
}
