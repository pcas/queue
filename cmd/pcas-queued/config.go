// Config.go handles configuration and logging for the pcas-queued server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"context"
	"errors"
	"fmt"
	"math"
	"os"
	"time"

	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/queue/queued"
	"bitbucket.org/pcas/queue/rabbitmq/rabbitmqflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
)

// Options describes the options.
type Options struct {
	Address           *address.Address // The address to bind to
	Lifetime          time.Duration    // The length of time before a message is automatically requeued
	SSLKey            []byte           // The SSL private key
	SSLKeyCert        []byte           // The SSL private key certificate
	MaxNumConnections int              // The maximum number of connections
	WithRabbitMQ      bool             // Enable RabbitMQ
}

// Name is the name of the executable.
const Name = "pcas-queued"

// The default values.
const (
	DefaultHostname          = "localhost"
	DefaultPort              = queued.DefaultTCPPort
	DefaultMaxNumConnections = 1024
	DefaultLifetime          = 24 * time.Hour
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default address
	addr, err := address.NewTCP(DefaultHostname, DefaultPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Return the default options
	return &Options{
		Address:           addr,
		MaxNumConnections: DefaultMaxNumConnections,
		Lifetime:          DefaultLifetime,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.MaxNumConnections <= 0 || opts.MaxNumConnections > math.MaxInt32 {
		return fmt.Errorf("invalid maximum number of connections (%d)", opts.MaxNumConnections)
	}
	n := 0
	if opts.WithRabbitMQ {
		n++
	}
	if n != 1 {
		return errors.New("-with-rabbitmq must be selected")
	}
	return nil
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// setMetrics starts the metrics reporting.
func setMetrics(sslClientSet *sslflag.ClientSet, metricsdbSet *metricsdbflag.MetricsSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Create the config
	c := metricsdbSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the metrics
	return metricsdbflag.SetMetrics(ctx, c, Name)
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s is the pcas queue server.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address, "The address to bind to", "The value of the flag -address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\"."),
		flag.Duration("lifetime", &opts.Lifetime, opts.Lifetime, "The length of time before a message is automatically requeued", ""),
		flag.Int("max-num-connections", &opts.MaxNumConnections, opts.MaxNumConnections, "The maximum number of connections", ""),
		flag.Bool("with-rabbitmq", &opts.WithRabbitMQ, opts.WithRabbitMQ, "Enable RabbitMQ", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the metricsdb flag set
	metricsdbSet := metricsdbflag.NewMetricsSet(nil)
	flag.AddSet(metricsdbSet)
	// Create and add the rabbitmq flag set
	rabbitmqSet := rabbitmqflag.NewSet(nil)
	flag.AddSet(rabbitmqSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Create and add the standard SSL server set
	sslServerSet := &sslflag.ServerSet{}
	flag.AddSet(sslServerSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL server details
	opts.SSLKey = sslServerSet.Key()
	opts.SSLKeyCert = sslServerSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	} else if !rabbitmqSet.Config().Address.IsTCP() {
		return errors.New("only TCP addresses are supported by RabbitMQ")
	}
	// Set the loggers
	if err := setLoggers(sslClientSet, logdSet); err != nil {
		return err
	}
	// Set the metrics
	if metricsdbSet.WithMetrics() {
		if err := setMetrics(sslClientSet, metricsdbSet); err != nil {
			return err
		}
	}
	// Set the default inmem and RabbitMQ configurations
	rabbitmqSet.SetDefault()
	return nil
}
