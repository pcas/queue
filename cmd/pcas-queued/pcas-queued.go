// Pcas-queued implements a queue server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"

	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/runtimemetrics"
	"bitbucket.org/pcas/queue/driver"
	"bitbucket.org/pcas/queue/queued"
	"bitbucket.org/pcas/queue/rabbitmq"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/listenutil"
	"bitbucket.org/pcastools/log"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createTCPListener returns a new TCP listener.
func createTCPListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := queued.DefaultTCPPort
	if a.HasPort() {
		port = a.Port()
	}
	return listenutil.TCPListener(a.Hostname(), port,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
}

// createWebsocketListener returns a new websocket listener.
func createWebsocketListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := queued.DefaultWSPort
	if a.HasPort() {
		port = a.Port()
	}
	uri := "ws://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
	l, shutdown, err := listenutil.WebsocketListenAndServe(uri,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
	if err != nil {
		return nil, err
	}
	cleanup.Add(shutdown)
	return l, nil
}

// createListener returns a new listener.
func createListener(opts *Options, lg log.Interface) (l net.Listener, err error) {
	switch opts.Address.Scheme() {
	case "tcp":
		l, err = createTCPListener(opts.Address, opts, lg)
	case "ws":
		l, err = createWebsocketListener(opts.Address, opts, lg)
	default:
		err = errors.New("unsupported URI scheme: " + opts.Address.Scheme())
	}
	if err != nil {
		lg.Printf("Error starting listener: %v", err)
	}
	return
}

// run starts up and runs the server. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, lg log.Interface, met metrics.Interface) error {
	// Set the driver
	var d driver.Interface
	var err error
	if opts.WithRabbitMQ {
		cfg := rabbitmq.DefaultConfig()
		cfg.Lifetime = opts.Lifetime
		cfg.Logger = log.PrefixWith(log.Log(), "[rabbitmq]")
		if d, err = rabbitmq.New(ctx, cfg); err != nil {
			return err
		}
	}
	serverOpts := []queued.Option{
		queued.WithDriver(d),
	}
	// Build the rest of the options
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, queued.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Create the server
	s, err := queued.NewServer(serverOpts...)
	if err != nil {
		return err
	}
	// Set the logger and the metrics endpoint
	s.SetLogger(log.PrefixWith(lg, "[pcas-queued]"))
	s.SetMetrics(met)
	// If the context fires, stop the server
	go func() {
		<-ctx.Done()
		s.GracefulStop()
	}()
	// Create the listener and serve any incoming connections
	l, err := createListener(opts, lg)
	if err != nil {
		return err
	}
	defer l.Close()
	return s.Serve(l)
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger and metrics
	opts := setOptions()
	lg := log.Log()
	mt := metrics.Metrics()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Record CPU usage etc
	defer func() {
		if e := runtimemetrics.Start(mt).Stop(); err == nil {
			err = e
		}
	}()
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the server
	err = run(ctx, opts, lg, mt)
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
