// Worker defines the refresh worker for the rabbitmq driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"bitbucket.org/pcas/queue/driver"
	qerrors "bitbucket.org/pcas/queue/errors"
	"bitbucket.org/pcas/rabbitmq"
	"bitbucket.org/pcastools/contextutil"
	"context"
	"errors"
	"fmt"
	"math/rand"
	"time"
)

// requestCode represents a type of request to the worker
type requestCode uint8

// request types
const (
	channelRequest = requestCode(iota)
	refreshChannelRequest
	emitRequest
	refreshRequest
	acknowledgeRequest
	requeueRequest
	deleteRequest
)

// request represents a request to the worker
type request struct {
	code    requestCode           // The type of request
	cancelC <-chan struct{}       // The channel for cancellation
	id      driver.ID             // The message ID, for refresh, acknowledge, and requeue requests
	queue   string                // The queue name, for channel or delete requests
	qid     int                   // The queue ID, for channel refresh requests
	ack     rabbitmq.Acknowledger // The acknowledger, for an emit request
}

// response represents a response to a request
type response struct {
	expiry time.Time                // The expiry time, for emit and refresh requests
	id     driver.ID                // The message ID, for emit requests
	c      <-chan *rabbitmq.Message // The message channel, for channel requests
	qid    int                      // The message channel ID, for channel requests
	err    error                    // The error, if any
}

////////////////////////////////////////////////////////////////////////////////
// Local functions
////////////////////////////////////////////////////////////////////////////////

// processRequest processes the request req and returns an appropriate response. Here lifetime is the length of time after which unacknowledged unrefreshed messages should be automatically requeued.
func (r *rabbit) processRequest(ctx context.Context, req *request, lifetime time.Duration) *response {
	// Bind the request lifetime to a context
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	defer contextutil.NewChannelLink(req.cancelC, cancel).Stop()
	// Process the request
	switch req.code {
	case channelRequest:
		return r.processChannel(ctx, req.queue)
	case refreshChannelRequest:
		return r.processRefreshChannel(ctx, req.queue, req.qid)
	case emitRequest:
		return r.processEmit(ctx, req.ack, lifetime)
	case refreshRequest:
		return r.processRefresh(ctx, req.id, lifetime)
	case acknowledgeRequest:
		return r.processAcknowledge(ctx, req.id)
	case requeueRequest:
		return r.processRequeue(ctx, req.id)
	case deleteRequest:
		return r.processDelete(ctx, req.queue)
	default:
		return &response{
			err: fmt.Errorf("internal error: unknown request code (%v)", req.code),
		}
	}
}

// processChannel processes a channel request for the given queue and returns an appropriate response.
func (r *rabbit) processChannel(_ context.Context, queue string) *response {
	// Do we already know this queue?
	c, ok := r.cs[queue]
	if !ok {
		// No -- create it
		r.lg.Printf("establishing connection to queue \"%s\"", queue)
		var cancel func()
		var err error
		c, cancel, err = r.conn.QueueConsume(queue)
		if err != nil {
			return &response{err: err}
		}
		r.cs[queue] = c
		r.qids[queue] = rand.Int()
		r.cancel_funcs[queue] = cancel
	}
	return &response{
		c:   c,
		qid: r.qids[queue],
	}
}

// processRefreshChannel processes a refresh channel request for the given queue and returns an appropriate response.
func (r *rabbit) processRefreshChannel(ctx context.Context, queue string, id int) *response {
	// Do we already know this queue? If so, close it
	if _, ok := r.cs[queue]; ok && r.qids[queue] == id {
		r.lg.Printf("removing stale connection to queue \"%s\"", queue)
		cancel := r.cancel_funcs[queue]
		cancel()
		delete(r.cs, queue)
		delete(r.qids, queue)
		delete(r.cancel_funcs, queue)
	}
	// Return the queue
	return r.processChannel(ctx, queue)
}

// processEmit processes an emit request with the given acknowledger and returns an appropriate response.
func (r *rabbit) processEmit(_ context.Context, ack rabbitmq.Acknowledger, lifetime time.Duration) *response {
	// Requeue stale messages
	r.pruneStack()
	// Create an ID for the message
	id, err := driver.NewID()
	for err != nil {
		time.Sleep(100 * time.Millisecond)
		id, err = driver.NewID()
	}
	// Record the expiry deadline and the ID
	m := &metadata{
		ack:    ack,
		expiry: time.Now().Add(lifetime),
		id:     id,
	}
	// Mark the message as awaiting acknowledgement
	present := r.stack.Insert(m.expiry, m)
	r.await[m.id] = m
	for present != nil {
		// There was a stack collision. We reinsert the item which was accidentally replaced. Note that, because we only change the expiry time of the *message that we reinsert, we do not need to update the map q.await.
		x := present.(*metadata)
		x.expiry = time.Now().Add(lifetime)
		present = r.stack.Insert(x.expiry, x)
	}
	// Return the response
	return &response{
		expiry: m.expiry,
		id:     m.id,
	}
}

// processRefresh processes a refresh request with the given id and returns an appropriate response. Here lifetime is the length of time after which an unacknowledged unrefreshed message should be automatically requeued.
func (r *rabbit) processRefresh(_ context.Context, id driver.ID, lifetime time.Duration) *response {
	// Requeue stale messages
	r.pruneStack()
	// Grab the metadata
	m, ok := r.await[id]
	if !ok {
		return &response{err: qerrors.UnknownMessage.New()}
	}
	// Remove it from the stack
	if r.stack.Remove(m.expiry, m) == nil {
		return &response{err: errors.New("internal error in processRefresh")}
	}
	// Reset the expiry time
	m.expiry = time.Now().Add(lifetime)
	// Put the message back on the stack
	present := r.stack.Insert(m.expiry, m)
	for present != nil {
		// There was a stack collision. We reinsert the item which was accidentally replaced. Note that, because we only change the expiry time of the *message that we reinsert, we do not need to update the map q.await.
		x := present.(*metadata)
		x.expiry = time.Now().Add(lifetime)
		present = r.stack.Insert(x.expiry, x)
	}
	// Return the response
	return &response{
		expiry: m.expiry,
	}
}

// processAcknowledge processes an acknowledge request with the given id and returns an appropriate response.
func (r *rabbit) processAcknowledge(_ context.Context, id driver.ID) *response {
	// Requeue stale messages
	r.pruneStack()
	// Grab the metadata
	m, ok := r.await[id]
	if !ok {
		return &response{err: qerrors.UnknownMessage.New()}
	}
	// Remove it from the stack
	if r.stack.Remove(m.expiry, m) == nil {
		return &response{err: errors.New("internal error in processAcknowledge")}
	}
	// Remove it from the map of messages awaiting acknowledgement
	delete(r.await, id)
	// Ack the message, ignoring any error
	if err := m.ack.Ack(); err != nil {
		r.lg.Printf("ignoring error when Acking message: %v", err)
	}
	// Return an empty response
	return &response{}
}

// processRequeue processes a requeue request with the given id and returns an appropriate response.
func (r *rabbit) processRequeue(_ context.Context, id driver.ID) *response {
	// Requeue stale messages
	r.pruneStack()
	// Grab the metadata
	m, ok := r.await[id]
	if !ok {
		return &response{err: qerrors.UnknownMessage.New()}
	}
	// Remove it from the stack
	if r.stack.Remove(m.expiry, m) == nil {
		return &response{err: errors.New("internal error in processRequeue")}
	}
	// Remove it from the map of messages awaiting acknowledgement
	delete(r.await, id)
	// Nack the message, ignoring any error
	if err := m.ack.Nack(); err != nil {
		r.lg.Printf("ignoring error when Nacking message: %v", err)
	}
	// Return an empty response
	return &response{}
}

// processDelete processes a delete request for the given queue and returns an appropriate response.
func (r *rabbit) processDelete(ctx context.Context, queue string) *response {
	// Recover the cancel function
	if cancel, ok := r.cancel_funcs[queue]; ok {
		cancel()
		delete(r.cs, queue)
		delete(r.qids, queue)
		delete(r.cancel_funcs, queue)
	}
	// Delete the queue
	n, err := r.conn.QueueDelete(ctx, queue, false, false)
	if err != nil {
		r.lg.Printf("error deleting queue: %v", err)
		return &response{err: err}
	}
	r.lg.Printf("deleted queue \"%s\" and %d messages", queue, n)
	return &response{}
}

// pruneStack requeues stale messages.
func (r *rabbit) pruneStack() {
	for _, x := range r.stack.Prune(time.Now()) {
		m, ok := x.(*metadata)
		if !ok {
			panic("fatal internal error in pruneStack") // this can't happen
		}
		// Delete m from the map of active messages
		delete(r.await, m.id)
		// Nack the message, ignoring any error
		if err := m.ack.Nack(); err != nil {
			r.lg.Printf("ignoring error when Nacking pruned message: %v", err)
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// Worker
////////////////////////////////////////////////////////////////////////////////

// worker handles refreshing and acknowledgement of messages. Intended to be run in its own goroutine. The argument lifetime is the length of time after which unacknowledged unrefereshed messages are automatically requeued.
func (r *rabbit) worker(lifetime time.Duration) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// main loop
mainLoop:
	for {
		// Wait for a request or shutdown.
		select {
		case req := <-r.requestC:
			// We process the request and emit a response
			r.responseC <- r.processRequest(ctx, req, lifetime)
		case <-r.shutdownC:
			// We shut down
			break mainLoop
		}
	}
	// Nack all the messages we know about, ignoring errors
	for _, m := range r.await {
		if err := m.ack.Nack(); err != nil {
			r.lg.Printf("ignoring error when Nacking message during shutdown: %v", err)
		}
	}
	// Signal that we have shut down
	close(r.doneC)
}
