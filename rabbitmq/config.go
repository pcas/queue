// Config handles configuration for the RabbitMQ queue driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/log"
	"errors"
	"sync"
	"time"
)

// Config describes the configuration options we allow a user to set on a RabbitMQ driver.
type Config struct {
	Lifetime time.Duration    // The length of time after which a message is automatically requeued
	Address  *address.Address // The address of the RabbitMQ server
	Username string           // The username for RabbitMQ
	Password string           // The password for RabbitMQ
	Logger   log.Interface    // The logger
}

// The default client configuration
const (
	DefaultLifetime = 5 * time.Minute
	DefaultHostname = "localhost"
	DefaultPort     = 5672
	DefaultUsername = "guest"
	DefaultPassword = "guest"
	DefaultLogger   = log.Discard
)

// The default values for the client configuration, along with controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *Config
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values.
func init() {
	// Construct the default server address
	addr, err := address.NewTCP(DefaultHostname, DefaultPort)
	if err != nil {
		panic(err)
	}
	defaults = &Config{
		Address:  addr,
		Lifetime: DefaultLifetime,
		Username: DefaultUsername,
		Password: DefaultPassword,
		Logger:   DefaultLogger,
	}
}

/////////////////////////////////////////////////////////////////////////
// Config functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default values.
func DefaultConfig() *Config {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultConfig.
func SetDefaultConfig(c *Config) *Config {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the configuration, returning an error if there's a problem.
func (c *Config) Validate() error {
	if c == nil {
		return errors.New("illegal nil configuration")
	} else if c.Lifetime <= 0 {
		return errors.New("the lifetime must be positive")
	} else if c.Address == nil {
		return errors.New("illegal nil address")
	} else if !c.Address.IsTCP() {
		return errors.New("the address must be a TCP address")
	} else if c.Address.Hostname() == "" {
		return errors.New("illegal empty hostname")
	} else if !c.Address.HasPort() {
		return errors.New("no port specified")
	} else if c.Password == "" {
		return errors.New("no password specified")
	} else if c.Username == "" {
		return errors.New("no username specified")
	}
	return nil
}

// Copy returns a copy of the configuration.
func (c *Config) Copy() *Config {
	return &Config{
		Lifetime: c.Lifetime,
		Address:  c.Address,
		Username: c.Username,
		Password: c.Password,
		Logger:   c.Logger,
	}
}

// URI returns the AQMP URI specified by c.
func (c *Config) URI() string {
	if c == nil {
		return ""
	}
	return "amqp://" + c.Username + ":" + c.Password + "@" + c.Address.HostnameAndPort()
}
