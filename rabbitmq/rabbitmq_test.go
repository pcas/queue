// rabbitmq_test provides tests for the rabbitmq package.

//go:build integration
// +build integration

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"bitbucket.org/pcas/queue/internal/drivertest"
	"context"
	"io"
	"testing"
	"time"
)

func TestAll(t *testing.T) {
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Create the confid
	cfg := DefaultConfig()
	cfg.Lifetime = time.Second
	// Connect to rabbitmq
	c, err := New(ctx, cfg)
	if err != nil {
		t.Fatalf("unable to create new rabbitmq queueing system: %v", err)
	}
	// Defer calling close (if necessary)
	defer func() {
		if cl, ok := c.(io.Closer); ok {
			if err := cl.Close(); err != nil {
				t.Fatalf("error on Close: %v", err)
			}
		}
	}()
	// Run the tests
	drivertest.Run(ctx, c, t)
}
