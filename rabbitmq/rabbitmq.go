// Rabbitmq defines a driver for the pcas queue system based on RabbitMQ.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"bitbucket.org/pcas/queue/driver"
	"bitbucket.org/pcas/queue/errors"
	"bitbucket.org/pcas/rabbitmq"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/timeutil"
	"context"
	"sync"
	"time"
)

// rabbit implements the driver.Interface for a RabbitMQ connection
type rabbit struct {
	conn      *rabbitmq.Connection // The connection to RabbitMQ
	pub       *rabbitmq.Publisher  // Publishes messages to queues on conn
	lg        log.Interface        // The logger
	requestC  chan *request        // Request channel to the refresh worker
	responseC chan *response       // Response channel from the refresh worker
	shutdownC chan interface{}     // Close to shut down the refresh worker
	doneC     chan interface{}     // Closes when the refresh worker exits

	// The following should only be accessed by the refresh worker
	stack        *timeutil.Stack                     // The stack of message metadata, ordered by expiry time
	await        map[driver.ID]*metadata             // A map from driver.IDs to message metadata
	cs           map[string]<-chan *rabbitmq.Message // A map from queue names to message channels
	qids         map[string]int                      // A map from queue names to channel IDs
	cancel_funcs map[string]func()                   // A map from queue names to channel cancel functions

	m        sync.Mutex // Mutex protecting the following
	isClosed bool
	closeErr error
}

// rabbit satisfies the driver.Interface
var _ driver.Interface = &rabbit{}

// metadata represents the parts of message data that we need to acknowledge, sort, and requeue messages.
type metadata struct {
	id     driver.ID             // Message ID
	ack    rabbitmq.Acknowledger // Acknowledger
	expiry time.Time             // Expiry time
}

// max_retry is the max number of times to retry building a channel connection
const max_retry = 6

////////////////////////////////////////////////////////////////////////////////
// Local functions
////////////////////////////////////////////////////////////////////////////////

// metadataCmp is the comparison function for message metadata. It returns true iff the id for message x is lexicographically less than that for y.
func metadataCmp(x interface{}, y interface{}) bool {
	return x.(*metadata).id.String() < y.(*metadata).id.String()
}

// wait returns after 2^attempt seconds or, if sooner, when the context fires.
func wait(ctx context.Context, attempt int) error {
	// We don't wait on our first attempt
	if attempt == 0 {
		return nil
	}
	// Compute the wait time
	var d time.Duration
	if attempt == 1 {
		d = 1 * time.Second
	} else {
		d = (2 << (attempt - 2)) * time.Second
	}
	// Perform the wait
	t := time.NewTimer(d)
	defer t.Stop()
	select {
	case <-t.C:
		// move on
	case <-ctx.Done():
		return ctx.Err()
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////
// rabbit functions
////////////////////////////////////////////////////////////////////////////////

// DriverName returns the name associated with this driver.
func (*rabbit) DriverName() string {
	return "rabbitmq"
}

// Close closes the connection to the RabbitMQ server.
func (r *rabbit) Close() error {
	r.m.Lock()
	defer r.m.Unlock()
	// Are we closed already?
	if !r.isClosed {
		// Shut down the worker
		close(r.shutdownC)
		<-r.doneC
		// Close the publisher
		if err := r.pub.Close(); err != nil {
			r.closeErr = err
		}
		// Call the cancel function for each known queue
		for _, cancel := range r.cancel_funcs {
			cancel()
		}
		// Close the connection
		if err := r.conn.Close(); err != nil {
			r.closeErr = err
		}
	}
	return r.closeErr
}

// Delete deletes the queue with the given name.
func (r *rabbit) Delete(ctx context.Context, queue string) error {
	// Create the request
	req := &request{
		code:    deleteRequest,
		cancelC: ctx.Done(),
		queue:   queue,
	}
	// Make the request
	select {
	case r.requestC <- req:
		// move on
	case <-ctx.Done():
		return ctx.Err()
	}
	// Wait for the response
	resp := <-r.responseC
	return resp.err
}

// Len returns the number of messages in the queue with the given name that are not awaiting acknowledgement.
func (r *rabbit) Len(ctx context.Context, queue string) (int, error) {
	n, err := r.conn.QueueLen(ctx, queue)
	if err != nil {
		r.lg.Printf("error getting length of queue \"%s\": %v", queue, err)
		return 0, err
	}
	return n, nil
}

// getChannel returns the message channel for the given queue, along with its ID.
func (r *rabbit) getChannel(ctx context.Context, queue string) (<-chan *rabbitmq.Message, int, error) {
	// Create the request
	req := &request{
		code:    channelRequest,
		cancelC: ctx.Done(),
		queue:   queue,
	}
	// Make the request
	select {
	case r.requestC <- req:
		// move on
	case <-ctx.Done():
		return nil, 0, ctx.Err()
	}
	// Wait for the response
	resp := <-r.responseC
	return resp.c, resp.qid, resp.err
}

// refreshChannel returns a fresh connection to the message channel for the given queue, along with its new ID.
func (r *rabbit) refreshChannel(ctx context.Context, queue string, id int) (<-chan *rabbitmq.Message, int, error) {
	// Create the request
	req := &request{
		code:    refreshChannelRequest,
		cancelC: ctx.Done(),
		queue:   queue,
		qid:     id,
	}
	// Make the request
	select {
	case r.requestC <- req:
		// move on
	case <-ctx.Done():
		return nil, 0, ctx.Err()
	}
	// Wait for the response
	resp := <-r.responseC
	return resp.c, resp.qid, resp.err
}

// getIDAndExpiry makes an emit request with the given acknowledger, returning the message ID and expiry time.
func (r *rabbit) getIDAndExpiry(ctx context.Context, ack rabbitmq.Acknowledger) (driver.ID, time.Time, error) {
	// Create the request
	req := &request{
		code:    emitRequest,
		cancelC: ctx.Done(),
		ack:     ack,
	}
	// Make the request
	select {
	case r.requestC <- req:
		// move on
	case <-ctx.Done():
		return driver.NilID, time.Time{}, ctx.Err()
	}
	// Wait for the response
	resp := <-r.responseC
	return resp.id, resp.expiry, resp.err
}

// Get returns the ID and content of the next message from the queue with the given name. The message will be automatically requeued if it is not acknowledged or refreshed by the returned time.
func (r *rabbit) Get(ctx context.Context, queue string) (driver.ID, []byte, time.Time, error) {
	// Grab the message channel
	c, qid, err := r.getChannel(ctx, queue)
	if err != nil {
		return driver.NilID, nil, time.Time{}, err
	}
	// Read from the message channel
	var attempt int
	var msg *rabbitmq.Message
	var ok bool
	for !ok {
		select {
		case msg, ok = <-c:
			// move on
		case <-ctx.Done():
			return driver.NilID, nil, time.Time{}, ctx.Err()
		}
		// If the channel has closed, we need to rebuild the connection
		if !ok {
			// Have we failed too many times? Otherwise wait before retrying
			if attempt >= max_retry {
				return driver.NilID, nil, time.Time{}, errors.UnableToConnectToQueue.New()
			} else if err = wait(ctx, attempt); err != nil {
				return driver.NilID, nil, time.Time{}, err
			}
			attempt++
			// Try and rebuild the connection
			c, qid, err = r.refreshChannel(ctx, queue, qid)
			if err != nil {
				return driver.NilID, nil, time.Time{}, err
			}
		}
	}
	// Get the ID and expiry time
	id, t, err := r.getIDAndExpiry(ctx, msg.Acknowledger())
	if err != nil {
		return driver.NilID, nil, time.Time{}, ctx.Err()
	}
	return id, msg.Body(), t, nil
}

// Put places a message with the given content on the queue with the given name.
func (r *rabbit) Put(ctx context.Context, queue string, content []byte) error {
	return r.pub.Publish(ctx, queue, content)
}

// Refresh gets a new expiry time for the message with the given ID.
func (r *rabbit) Refresh(ctx context.Context, id driver.ID) (time.Time, error) {
	// Create the request
	req := &request{
		code:    refreshRequest,
		cancelC: ctx.Done(),
		id:      id,
	}
	// Make the request
	select {
	case r.requestC <- req:
		// move on
	case <-ctx.Done():
		return time.Time{}, ctx.Err()
	}
	// Wait for the response
	resp := <-r.responseC
	return resp.expiry, resp.err
}

// Acknowledge removes the message with the given ID from its queue.
func (r *rabbit) Acknowledge(ctx context.Context, id driver.ID) error {
	// Create the request
	req := &request{
		code:    acknowledgeRequest,
		cancelC: ctx.Done(),
		id:      id,
	}
	// Make the request
	select {
	case r.requestC <- req:
		// move on
	case <-ctx.Done():
		return ctx.Err()
	}
	// Wait for the response
	resp := <-r.responseC
	return resp.err
}

// Requeue requeues the message with the given ID.
func (r *rabbit) Requeue(ctx context.Context, id driver.ID) error {
	// Create the request
	req := &request{
		code:    requeueRequest,
		cancelC: ctx.Done(),
		id:      id,
	}
	// Make the request
	select {
	case r.requestC <- req:
		// move on
	case <-ctx.Done():
		return ctx.Err()
	}
	// Wait for the response
	resp := <-r.responseC
	return resp.err
}

// New returns a new queue server based on RabbitMQ with the given configuration
func New(ctx context.Context, cfg *Config) (driver.Interface, error) {
	// Validate the config
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Connect to RabbitMQ
	conn, err := rabbitmq.Connect(ctx, cfg.URI(), cfg.Logger)
	if err != nil {
		return nil, err
	}
	// Create a publisher
	pub, err := conn.CreatePublisher()
	if err != nil {
		return nil, err
	}
	// Create the result
	r := &rabbit{
		conn:         conn,
		pub:          pub,
		lg:           cfg.Logger,
		requestC:     make(chan *request),
		responseC:    make(chan *response),
		shutdownC:    make(chan interface{}),
		doneC:        make(chan interface{}),
		stack:        timeutil.NewStack(metadataCmp),
		await:        make(map[driver.ID]*metadata),
		cs:           make(map[string]<-chan *rabbitmq.Message),
		qids:         make(map[string]int),
		cancel_funcs: make(map[string]func()),
	}
	// Launch the refresh worker
	go r.worker(cfg.Lifetime)
	return r, nil
}
