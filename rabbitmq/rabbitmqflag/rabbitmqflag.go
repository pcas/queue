// Rabbitmqflag provides a standard set of command line flags for the rabbitmq driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmqflag

import (
	"errors"
	"fmt"
	"os"

	"bitbucket.org/pcas/queue/rabbitmq"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
)

// The environment variables consulted
const (
	EnvUsername = "PCAS_RABBITMQ_USERNAME"
	EnvPassword = "PCAS_RABBITMQ_PASSWORD"
	EnvAddress  = "PCAS_RABBITMQ_ADDRESS"
)

// Descriptions of environment variables.
const (
	unset       = "unset"
	setButEmpty = "set, but empty"
)

// passwordFlag defines a flag for setting the RabbitMQ password. It satisfies the flag.Flag interface.
type passwordFlag struct {
	password *string // The password
}

// usernameFlag defines a flag for setting the RabbitMQ username. It satisfies the flag.Flag interface.
type usernameFlag struct {
	username *string // The username
}

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c     *rabbitmq.Config // The config
	flags []flag.Flag      // The flags
}

/////////////////////////////////////////////////////////////////////////
// passwordFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*passwordFlag) Name() string {
	return "rabbitmq-password"
}

// Description returns a one-line description of this variable.
func (f *passwordFlag) Description() string {
	return fmt.Sprintf("The RabbitMQ password (default: \"%s\")", *f.password)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *passwordFlag) Usage() string {
	// read the environment variable
	status := "set"
	if val, ok := os.LookupEnv(EnvPassword); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvPassword, status)
}

// Parse parses the string.
func (f *passwordFlag) Parse(in string) error {
	*f.password = in
	return nil
}

// newPasswordFlag returns a new passwordFlag. It has backing variable password.
func newPasswordFlag(password *string) flag.Flag {
	return &passwordFlag{
		password: password,
	}
}

/////////////////////////////////////////////////////////////////////////
// usernameFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*usernameFlag) Name() string {
	return "rabbitmq-username"
}

// Description returns a one-line description of this variable.
func (f *usernameFlag) Description() string {
	return fmt.Sprintf("The RabbitMQ username (default: \"%s\")", *f.username)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *usernameFlag) Usage() string {
	// read the environment variable
	var status string
	if val, ok := os.LookupEnv(EnvUsername); !ok {
		status = unset
	} else if len(val) == 0 {
		status = setButEmpty
	} else {
		status = "\"" + val + "\""
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (currently %s).", f.Name(), EnvUsername, status)
}

// Parse parses the string.
func (f *usernameFlag) Parse(in string) error {
	*f.username = in
	return nil
}

// newUsernameFlag returns a new usernameFlag. It has backing variable username.
func newUsernameFlag(username *string) flag.Flag {
	return &usernameFlag{
		username: username,
	}
}

/////////////////////////////////////////////////////////////////////////
// Set functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the rabbitmq.DefaultConfig() will be used. Note that this does not update the default config, nor does this update c. To recover the updated config after parse, call the Config() method on the returned set.
func NewSet(c *rabbitmq.Config) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = rabbitmq.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the flags
	flags := []flag.Flag{
		address.NewFlag(
			"rabbitmq-address",
			&c.Address, c.Address,
			"The address of the pcas kvdb server",
			address.EnvUsage("rabbitmq-address", EnvAddress),
		),
		newPasswordFlag(&c.Password),
		newUsernameFlag(&c.Username),
	}
	// Return the set
	return &Set{
		c:     c,
		flags: flags,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	flags := make([]flag.Flag, len(s.flags))
	copy(flags, s.flags)
	return flags
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "RabbitMQ options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// Config returns the config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) Config() *rabbitmq.Config {
	if s == nil {
		return rabbitmq.DefaultConfig()
	}
	return s.c.Copy()
}

// SetDefault sets the default config as described by this set. This should only be called after the set has been successfully validated.
func (s *Set) SetDefault() {
	c := s.Config()
	rabbitmq.SetDefaultConfig(c)
}
