// Interface defines the interface for a queue driver.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package driver

import (
	"bitbucket.org/pcastools/ulid"
	"context"
	"time"
)

// ID represents a unique ID for a message.
type ID ulid.ULID

// NilID is a special ID.
var NilID = ID(ulid.Nil)

// Interface is the interface satisfied by a queuing system.
type Interface interface {
	// DriverName returns the name associated with this driver.
	DriverName() string
	// Delete deletes the queue with the given name.
	Delete(context.Context, string) error
	// Len returns the number of messages in the queue with the given name
	// that are not awaiting acknowledgement.
	Len(context.Context, string) (int, error)
	// Get returns the ID and content of the next message from the queue
	// with the given name. The message will be automatically requeued if
	// it is not acknowledged or refreshed by the returned time.
	Get(context.Context, string) (ID, []byte, time.Time, error)
	// Put places a message with the given content on the queue with the
	// given name.
	Put(context.Context, string, []byte) error
	// Refresh gets a new expiry time for the message with the given ID.
	Refresh(context.Context, ID) (time.Time, error)
	// Acknowledge removes the message with the given ID from its queue.
	Acknowledge(context.Context, ID) error
	// Requeue requeues the message with the given ID.
	Requeue(context.Context, ID) error
}

////////////////////////////////////////////////////////////////////////////////
// ID functions
////////////////////////////////////////////////////////////////////////////////

// String returns a string representation of id.
func (id ID) String() string {
	return ulid.ULID(id).String()
}

// Hash returns a hash of id.
func (id ID) Hash() uint32 {
	return ulid.ULID(id).Hash()
}

// NewID returns a new id.
func NewID() (ID, error) {
	// Hand off to the ULID package
	u, err := ulid.New()
	if err != nil {
		return NilID, err
	}
	// Convert the ULID
	return ID(u), nil
}

// NewIDFromString returns a new id based on the string s, which should be a ULID.
func NewIDFromString(s string) (ID, error) {
	u, err := ulid.FromString(s)
	if err != nil {
		return NilID, err
	}
	return ID(u), nil
}
