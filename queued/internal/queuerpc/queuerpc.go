//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. queue.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queuerpc

import (
	"errors"
	"time"

	"bitbucket.org/pcas/queue/driver"
	"google.golang.org/protobuf/types/known/timestamppb"
)

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromString converts a string to a *QueueName.
func FromString(name string) *QueueName {
	return &QueueName{Name: name}
}

// ToString converts a *QueueName to a string.
func ToString(qn *QueueName) (string, error) {
	if qn == nil {
		return "", errors.New("nil *QueueName in ToString")
	}
	return qn.GetName(), nil
}

// FromInt converts an int to a *QueueLength.
func FromInt(n int) *QueueLength {
	return &QueueLength{Len: int64(n)}
}

// ToInt converts a *QueueLength to an int.
func ToInt(ql *QueueLength) (int, error) {
	if ql == nil {
		return 0, errors.New("nil *QueueLength in ToInt")
	}
	return int(ql.GetLen()), nil
}

// FromID converts a driver.ID to a *ID.
func FromID(id driver.ID) *ID {
	return &ID{Id: id.String()}
}

// ToID converts a *ID to a driver.ID.
func ToID(id *ID) (driver.ID, error) {
	if id == nil {
		return driver.NilID, errors.New("nil *ID in ToID")
	}
	return driver.NewIDFromString(id.GetId())
}

// FromBytes converts a slice of bytes to a *Content.
func FromBytes(b []byte) *Content {
	if b == nil {
		b = []byte{}
	}
	return &Content{Content: b}
}

// ToBytes converts a *Content to a slice of bytes.
func ToBytes(c *Content) ([]byte, error) {
	if c == nil {
		return nil, errors.New("nil *Content in ToBytes")
	}
	return []byte(c.GetContent()), nil
}

// FromTime converts a time.Time to a *ExpiryTime.
func FromTime(t time.Time) *ExpiryTime {
	return &ExpiryTime{
		Time: timestamppb.New(t),
	}
}

// ToTime converts a *ExpiryTime to a time.Time.
func ToTime(et *ExpiryTime) (time.Time, error) {
	if et == nil {
		return time.Time{}, errors.New("nil *ExpiryTime in ToTime")
	}
	return et.GetTime().AsTime(), nil
}

// FromIDBytesAndTime converts an ID, a slice of bytes, and an expiry time to a *Message.
func FromIDBytesAndTime(id driver.ID, b []byte, t time.Time) *Message {
	return &Message{
		Id:      FromID(id),
		Content: FromBytes(b),
		Expiry:  FromTime(t),
	}
}

// ToIDBytesAndTime converts a *Message to a driver.ID, a slice of bytes, and a time.Time.
func ToIDBytesAndTime(m *Message) (driver.ID, []byte, time.Time, error) {
	if m == nil {
		return driver.NilID, nil, time.Time{}, errors.New("nil *Message in ToMessage")
	}
	// Convert the ID
	id, err := ToID(m.GetId())
	if err != nil {
		return driver.NilID, nil, time.Time{}, err
	}
	// Convert the content
	b, err := ToBytes(m.GetContent())
	if err != nil {
		return driver.NilID, nil, time.Time{}, err
	}
	// Convert the expiry time
	t, err := ToTime(m.GetExpiry())
	if err != nil {
		return driver.NilID, nil, time.Time{}, err
	}
	return id, b, t, nil
}

// FromBytesAndString converts a slice of bytes and a string to a *ContentAndQueueName.
func FromBytesAndString(b []byte, name string) *ContentAndQueueName {
	return &ContentAndQueueName{
		Content: FromBytes(b),
		Name:    FromString(name),
	}
}

// ToBytesAndString converts a *ContentAndQueueName to a slice of bytes and a string.
func ToBytesAndString(cq *ContentAndQueueName) ([]byte, string, error) {
	if cq == nil {
		return nil, "", errors.New("nil *ContentAndQueueName in ToBytesAndString")
	}
	// Convert the content
	b, err := ToBytes(cq.GetContent())
	if err != nil {
		return nil, "", err
	}
	// Convert the queue name
	name, err := ToString(cq.GetName())
	if err != nil {
		return nil, "", err
	}
	return b, name, nil
}
