// Clientoptions provides options parsing for the queued client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queued

import (
	"errors"
	"os"
	"strconv"
	"sync"

	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/hash"
)

// ErrNilConfiguration is returned when a ClientConfig fails to validate.
var ErrNilConfiguration = errors.New("illegal nil configuration data")

// ClientConfig describes the configuration options we allow a user to set on a queue client.
type ClientConfig struct {
	Address     *address.Address // The address to connect to
	SSLCert     []byte           // The SSL certificate(s) (if any)
	SSLDisabled bool             // Disable SSL?
}

// The default values for the client configuration, and its controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaultC  *ClientConfig
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values.
func init() {
	// Create the default addresses
	addr, err := address.NewTCP("localhost", DefaultTCPPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Create the initial defaults
	defaultC = &ClientConfig{
		Address: addr,
	}
	// Modify the defaults based on environment variables
	if val, ok := os.LookupEnv("PCAS_QUEUE_ADDRESS"); ok {
		addr, err := address.New(val)
		if err == nil {
			defaultC.Address = addr
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// ClientConfig functions
/////////////////////////////////////////////////////////////////////////

// Validate validates the client configuration, returning an error if there's a problem.
func (c *ClientConfig) Validate() error {
	if c == nil || c.Address == nil {
		return ErrNilConfiguration
	}
	return nil
}

// URI returns the URI connection string.
//
// Note that the returned string may differ slightly from the value returned c.Address.URI(). For example, if c.Address omits a port number, an appropriate default port number is used here.
func (c *ClientConfig) URI() string {
	a := c.Address
	// Get the port number
	port := a.Port()
	if !a.HasPort() {
		switch a.Scheme() {
		case "ws":
			port = DefaultWSPort
		default:
			port = DefaultTCPPort
		}
	}
	// Return the uri
	return a.Scheme() + "://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
}

// Copy returns a copy of the configuration.
func (c *ClientConfig) Copy() *ClientConfig {
	cc := *c
	// Make a copy of the SSLCert
	if c.SSLCert != nil {
		crt := make([]byte, len(c.SSLCert))
		copy(crt, c.SSLCert)
		cc.SSLCert = crt
	}
	return &cc
}

// DefaultConfig returns a new client configuration initialised with the default values.
//
// The initial default value for the hostname will be read from the environment variable PCAS_QUEUE_ADDRESS on package init. This should take the form "hostname[:port]" or "ws://host/path".
func DefaultConfig() *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaultC.Copy()
}

// SetDefaultConfig sets the default client configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultClientConfig.
func SetDefaultConfig(c *ClientConfig) *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaultC
	defaultC = c.Copy()
	return oldc
}

// Hash returns a hash for the configuration.
func (c *ClientConfig) Hash() uint32 {
	h := hash.String(c.URI())
	h = hash.Combine(h, hash.Bool(c.SSLDisabled))
	if !c.SSLDisabled {
		h = hash.Combine(h, hash.ByteSlice(c.SSLCert))
	}
	return h
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// AreEqual returns true iff the two configurations are equal.
func AreEqual(c *ClientConfig, d *ClientConfig) bool {
	if c.SSLDisabled != d.SSLDisabled ||
		c.URI() != d.URI() {
		return false
	}
	if !c.SSLDisabled {
		if len(c.SSLCert) != len(d.SSLCert) {
			return false
		}
		for i, b := range c.SSLCert {
			if d.SSLCert[i] != b {
				return false
			}
		}
	}
	return true
}
