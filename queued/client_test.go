// Client_test provides tests for the queued package.

//go:build integration
// +build integration

/*
This test requires RabbitMQ to be running and listening on localhost:5672.

To run, do:
go test -tags=integration
*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queued

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io"
	"math/big"
	"net"
	"strconv"
	"testing"
	"time"

	"bitbucket.org/pcas/queue/internal/drivertest"
	"bitbucket.org/pcas/queue/rabbitmq"
	"bitbucket.org/pcastools/address"
	"github.com/stretchr/testify/require"
)

const (
	testSSLCert = "PCAS_SSL_KEY_CERT"
	testSSLKey  = "PCAS_SSL_KEY"
	testAddr    = "localhost"
	testPort    = 45559
)

func generateSSLCertAndKey() (cert []byte, key []byte, err error) {
	// Make our certificate
	c := &x509.Certificate{
		SerialNumber: big.NewInt(1),
		DNSNames:     []string{"localhost"},
		NotBefore:    time.Now(),
		NotAfter:     time.Now().Add(5 * time.Minute),
	}
	// Generate the private key
	certPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, nil, err
	}
	// Self-sign the certificate
	certBytes, err := x509.CreateCertificate(rand.Reader, c, c, &certPrivKey.PublicKey, certPrivKey)
	if err != nil {
		return nil, nil, err
	}
	// Encode the certificate and key
	certPEM := new(bytes.Buffer)
	pem.Encode(certPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: certBytes,
	})
	certPrivKeyPEM := new(bytes.Buffer)
	pem.Encode(certPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(certPrivKey),
	})
	// Return the encoded certificate and key
	return certPEM.Bytes(), certPrivKeyPEM.Bytes(), nil
}

func startServer(ctx context.Context, cert []byte, key []byte) (<-chan struct{}, error) {
	// Establish a connection
	l, err := net.Listen("tcp", testAddr+":"+strconv.Itoa(testPort))
	if err != nil {
		return nil, err
	}
	// Connect to RabbitMQ
	cfg := rabbitmq.DefaultConfig()
	cfg.Lifetime = time.Second
	// Connect to rabbitmq
	c, err := rabbitmq.New(ctx, cfg)
	if err != nil {
		return nil, err
	}
	// Create the server
	s, err := NewServer(
		WithDriver(c),
		SSLCertAndKey(cert, key),
	)
	if err != nil {
		if cl, ok := c.(io.Closer); ok {
			cl.Close()
		}
		l.Close()
		return nil, err
	}
	// Run the server in a new goroutine
	doneC := make(chan struct{})
	go func() {
		// Defer cleanup
		defer func() {
			if cl, ok := c.(io.Closer); ok {
				cl.Close()
			}
			l.Close()
			close(doneC)
		}()
		// Stop the server when the context fires
		go func() {
			<-ctx.Done()
			s.GracefulStop()
		}()
		// Start serving
		s.Serve(l)
	}()
	return doneC, nil
}

func connectClient(ctx context.Context, cert []byte) (*Client, error) {
	// Create the address
	addr, err := address.NewTCP(testAddr, testPort)
	if err != nil {
		return nil, err
	}
	// Create the client config
	cfg := DefaultConfig()
	cfg.Address = addr
	cfg.SSLCert = cert
	// Open the client connection
	return NewClient(ctx, cfg)
}

func TestAll(t *testing.T) {
	// Make a temporary SSL certificate and key
	cert, key, err := generateSSLCertAndKey()
	require.NoError(t, err)
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	// Start the server
	doneC, err := startServer(ctx, cert, key)
	if err != nil {
		cancel()
		t.Fatalf("unable to start server: %v", err)
	}
	defer func() {
		cancel()
		<-doneC
	}()
	// Open a client connection
	c, err := connectClient(ctx, cert)
	if err != nil {
		t.Fatalf("unable to create new client: %v", err)
	}
	// Run the tests
	drivertest.Run(ctx, c, t)
	// Close the client
	if err = c.Close(); err != nil {
		t.Fatalf("error closing client: %v", err)
	}
}
