// Serveroptions provides configuration options for a queued server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queued

import (
	"bitbucket.org/pcas/queue/driver"
	"errors"
)

// Option sets options on a queued server.
type Option interface {
	apply(*serverOptions) error
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*serverOptions) error
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *serverOptions) error {
	return h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*serverOptions) error) *funcOption {
	return &funcOption{
		f: f,
	}
}

// serverOptions are the options on a queued server.
type serverOptions struct {
	Driver  driver.Interface // The underlying queue system
	SSLCert []byte           // The SSL (public) certificate
	SSLKey  []byte           // The SSL (private) key
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) (*serverOptions, error) {
	// Create the default options
	opts := &serverOptions{}
	// Set the options
	for _, h := range options {
		if err := h.apply(opts); err != nil {
			return nil, err
		}
	}
	// Check we have a driver
	if opts.Driver == nil {
		return nil, errors.New("missing driver")
	}
	// Looks good
	return opts, nil
}

// SSLCertAndKey adds the given SSL public certificate and private key to the server.
func SSLCertAndKey(crt []byte, key []byte) Option {
	// Copy the certificate and key
	crtCopy := make([]byte, len(crt))
	copy(crtCopy, crt)
	keyCopy := make([]byte, len(key))
	copy(keyCopy, key)
	// Return the option
	return newFuncOption(func(opts *serverOptions) error {
		if len(crtCopy) == 0 {
			return errors.New("missing SSL certificate")
		} else if len(keyCopy) == 0 {
			return errors.New("missing SSL private key")
		}
		opts.SSLCert = crtCopy
		opts.SSLKey = keyCopy
		return nil
	})
}

// WithDriver sets the underlying queue system used by the server.
func WithDriver(d driver.Interface) Option {
	return newFuncOption(func(opts *serverOptions) error {
		opts.Driver = d
		return nil
	})
}
