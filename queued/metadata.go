// Metadata defines functions that encode errors using gRPC metadata.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queued

import (
	qerrors "bitbucket.org/pcas/queue/errors"
	"bitbucket.org/pcastools/grpcutil"
	"context"
	"errors"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"strconv"
)

// errorToMetadata encodes details about the given error as metadata.
func errorToMetadata(err error) metadata.MD {
	md := make(map[string]string)
	// Add the error message
	md["error"] = err.Error()
	// Add the error code, if present
	if e, ok := err.(qerrors.Error); ok {
		md["error_code"] = strconv.Itoa(int(e.Code()))
	}
	return metadata.New(md)
}

// metadataToError decodes details about an error from the given metadata.
func metadataToError(md metadata.MD) (err error) {
	// Extract the error message
	var msg string
	S := md.Get("error")
	if len(S) == 0 {
		return // Nothing to do
	}
	msg = S[0]
	// Extract and return the error code, if present
	if S := md.Get("error_code"); len(S) != 0 {
		if c, err := strconv.Atoi(S[0]); err == nil {
			return qerrors.Code(c).New()
		}
	}
	return errors.New(msg)
}

// unaryServerErrorInterceptor is a unary server interceptor for encoding errors in the trailer metadata.
func unaryServerErrorInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in %s: %v", info.FullMethod, e)
		}
	}()
	resp, err = handler(ctx, req)
	if err != nil {
		err = grpc.SetTrailer(ctx, errorToMetadata(err))
	}
	return
}

// streamServerErrorInterceptor is a stream server interceptor for encoding errors in the trailer metadata.
func streamServerErrorInterceptor(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in %s: %v", info.FullMethod, e)
		}
	}()
	err = handler(srv, stream)
	if err != nil {
		stream.SetTrailer(errorToMetadata(err))
		err = nil
	}
	return
}

// unaryClientErrorInterceptor is a unary client interceptor for extracting errors from the trailer metadata.
func unaryClientErrorInterceptor(ctx context.Context, method string, req interface{}, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	trailer := metadata.MD{}
	opts = append(opts, grpc.Trailer(&trailer))
	err := invoker(ctx, method, req, reply, cc, opts...)
	if err == nil {
		err = metadataToError(trailer)
	}
	return grpcutil.ConvertError(err)
}
