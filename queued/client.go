// Client describes the client view of the queued server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queued

import (
	"bitbucket.org/pcas/queue/driver"
	"bitbucket.org/pcas/queue/queued/internal/queuerpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/grpcutil/grpcs2"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"net/url"
	"strconv"
	"sync"
	"time"
)

// Client is the client view of the server.
type Client struct {
	log.BasicLogable
	io.Closer
	client   queuerpc.QueuedClient // The gRPC client
	m        sync.RWMutex          // mutex protecting isClosed and closeErr
	isClosed bool                  // true if and only if the worker is closed
	closeErr error                 // the error, if any, on close
}

// errors
var (
	ErrNilClient = errors.New("uninitialised client")
	ErrClosed    = errors.New("client is closed")
)

// Assert that the client satisfies the interface for a queueing system
var _ driver.Interface = &Client{}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDialFunc returns the dial function for the client. Assumes that the URI has scheme "tcp" or "ws", and that a port number is set.
func createDialFunc(uri string, lg log.Interface) grpc.DialOption {
	// Parse the URI
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		panic("invalid URI: " + uri)
	}
	switch u.Scheme {
	case "tcp":
		// Connect via a TCP socket
		port, err := strconv.Atoi(u.Port())
		if err != nil {
			panic("invalid URI: " + uri)
		}
		return grpcdialer.TCPDialer(u.Hostname(), port, lg)
	case "ws":
		// Connect via a websocket
		return grpcdialer.WebSocketDialer(uri, lg)
	default:
		// Unknown scheme
		panic("unsupported URI scheme: " + u.Scheme)
	}
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// NewClient returns a new client view of the server specified by the configuration cfg.
func NewClient(ctx context.Context, cfg *ClientConfig) (*Client, error) {
	// Sanity checks
	if cfg == nil {
		cfg = DefaultConfig()
	}
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create a new client
	c := &Client{}
	// Set the dial options
	dialOptions := []grpc.DialOption{
		createDialFunc(cfg.URI(), c.Log()),
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcs2.Name),
		),
		grpc.WithChainUnaryInterceptor(
			unaryClientErrorInterceptor,
		),
	}
	// Add the transport credentials
	var creds credentials.TransportCredentials
	if cfg.SSLDisabled {
		creds = insecure.NewCredentials()
	} else {
		creds = grpcutil.NewClientTLS(cfg.SSLCert)
	}
	dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, cfg.Address.Hostname(), dialOptions...)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = queuerpc.NewQueuedClient(conn)
	return c, nil
}

// Close closes the client.
func (c *Client) Close() error {
	if c == nil {
		return nil
	}
	c.m.Lock()
	defer c.m.Unlock()
	if !c.isClosed {
		// Close the server connection and record any error
		if err := c.Closer.Close(); err != nil {
			c.closeErr = err
		}
		c.isClosed = true
	}
	return c.closeErr
}

// isNilOrClosed returns an appropriate error if c is nil or closed
func (c *Client) isNilOrClosed() error {
	if c == nil {
		return ErrNilClient
	}
	c.m.RLock()
	defer c.m.RUnlock()
	if c.isClosed {
		return ErrClosed
	}
	return nil
}

// Delete deletes the queue with the given name.
func (c *Client) Delete(ctx context.Context, name string) error {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return err
	}
	// Hand off to the server
	_, err := c.client.Delete(ctx, queuerpc.FromString(name))
	return err
}

// Len returns the number of messages in the queue with the given name that are not awaiting acknowledgement.
func (c *Client) Len(ctx context.Context, name string) (int, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return 0, err
	}
	// Hand off to the server
	ql, err := c.client.Len(ctx, queuerpc.FromString(name))
	if err != nil {
		return 0, err
	}
	// Convert the return value
	return queuerpc.ToInt(ql)
}

// Get returns the ID, content, and expiry time of the next message from the queue with the given name. The message will, eventually, be automatically requeued if it is not acknowledged.
func (c *Client) Get(ctx context.Context, name string) (driver.ID, []byte, time.Time, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return driver.NilID, nil, time.Time{}, err
	}
	// Hand off to the server
	m, err := c.client.Get(ctx, queuerpc.FromString(name))
	if err != nil {
		return driver.NilID, nil, time.Time{}, err
	}
	// Convert the return value
	return queuerpc.ToIDBytesAndTime(m)
}

// Put places a message with the given content on the queue with the given name.
func (c *Client) Put(ctx context.Context, name string, content []byte) error {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return err
	}
	// Hand off to the server
	_, err := c.client.Put(ctx, queuerpc.FromBytesAndString(content, name))
	return err
}

// Refresh gets a new expiry time for the message with the given ID.
func (c *Client) Refresh(ctx context.Context, id driver.ID) (time.Time, error) {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return time.Time{}, err
	}
	// Hand off to the server
	t, err := c.client.Refresh(ctx, queuerpc.FromID(id))
	if err != nil {
		return time.Time{}, err
	}
	// Convert the return value
	return queuerpc.ToTime(t)
}

// Acknowledge removes the message with the given ID from its queue.
func (c *Client) Acknowledge(ctx context.Context, id driver.ID) error {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return err
	}
	// Hand off to the server
	_, err := c.client.Acknowledge(ctx, queuerpc.FromID(id))
	return err
}

// Requeue requeues the message with the given ID.
func (c *Client) Requeue(ctx context.Context, id driver.ID) error {
	// Sanity check
	if err := c.isNilOrClosed(); err != nil {
		return err
	}
	// Hand off to the server
	_, err := c.client.Requeue(ctx, queuerpc.FromID(id))
	return err
}

// DriverName returns "pcas queued client".
func (c *Client) DriverName() string {
	return "pcas queued client"
}
