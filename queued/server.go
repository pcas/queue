// Server handles connections from a queued client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queued

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/grpcmetrics"
	"bitbucket.org/pcas/queue/driver"
	"bitbucket.org/pcas/queue/queued/internal/queuerpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/protobuf/types/known/emptypb"

	// Make our compressors available
	_ "bitbucket.org/pcastools/grpcutil/grpcs2"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
	_ "google.golang.org/grpc/encoding/gzip"
)

// The default ports that the queued server listens on.
const (
	DefaultTCPPort = 12361
	DefaultWSPort  = 80
)

// queuedServer implements the tasks on the gRPC server.
type queuedServer struct {
	queuerpc.UnimplementedQueuedServer
	log.BasicLogable
	metrics.BasicMetricsable
	d driver.Interface // The underlying queue system
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	metrics.SetMetricser
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// queuedServer functions
/////////////////////////////////////////////////////////////////////////

// Delete deletes the queue with the given name.
func (s *queuedServer) Delete(ctx context.Context, qn *queuerpc.QueueName) (*emptypb.Empty, error) {
	if qn == nil {
		return &emptypb.Empty{}, errors.New("nil queue specification in Delete")
	}
	// Convert the queue name
	name, err := queuerpc.ToString(qn)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the underlying driver
	return &emptypb.Empty{}, s.d.Delete(ctx, name)

}

// Len returns the number of messages in the queue with the given name that are not awaiting acknowledgement.
func (s *queuedServer) Len(ctx context.Context, qn *queuerpc.QueueName) (*queuerpc.QueueLength, error) {
	if qn == nil {
		return &queuerpc.QueueLength{}, errors.New("nil queue specification in Len")
	}
	// Convert the queue name
	name, err := queuerpc.ToString(qn)
	if err != nil {
		return &queuerpc.QueueLength{}, err
	}
	// Hand off to the underlying driver
	n, err := s.d.Len(ctx, name)
	if err != nil {
		return &queuerpc.QueueLength{}, err
	}
	// Convert the return value
	return queuerpc.FromInt(n), nil
}

// Get returns the next message from the queue with the given name. The message will, eventually, be automatically requeued if it is not acknowledged.
func (s *queuedServer) Get(ctx context.Context, qn *queuerpc.QueueName) (*queuerpc.Message, error) {
	if qn == nil {
		return &queuerpc.Message{}, errors.New("nil queue specification in Get")
	}
	// Convert the queue name
	name, err := queuerpc.ToString(qn)
	if err != nil {
		return &queuerpc.Message{}, err
	}
	// Hand off to the underlying driver
	id, content, expiry, err := s.d.Get(ctx, name)
	if err != nil {
		return &queuerpc.Message{}, err
	}
	// Convert the return value
	return queuerpc.FromIDBytesAndTime(id, content, expiry), nil
}

// Put places a message with the given content on the queue with the given name.
func (s *queuedServer) Put(ctx context.Context, cq *queuerpc.ContentAndQueueName) (*emptypb.Empty, error) {
	if cq == nil {
		return &emptypb.Empty{}, errors.New("nil message specification in Put")
	}
	// Convert the content and queue name
	b, name, err := queuerpc.ToBytesAndString(cq)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the underlying driver
	return &emptypb.Empty{}, s.d.Put(ctx, name, b)
}

// Refresh gets a new expiry time for the message with the given ID.
func (s *queuedServer) Refresh(ctx context.Context, id *queuerpc.ID) (*queuerpc.ExpiryTime, error) {
	if id == nil {
		return &queuerpc.ExpiryTime{}, errors.New("nil id specification in Refresh")
	}
	// Convert the id
	x, err := queuerpc.ToID(id)
	if err != nil {
		return &queuerpc.ExpiryTime{}, err
	}
	// Hand off to the underlying driver
	expiry, err := s.d.Refresh(ctx, x)
	if err != nil {
		return &queuerpc.ExpiryTime{}, err
	}
	// Convert the return value
	return queuerpc.FromTime(expiry), nil
}

// Acknowledge removes the message with the given ID from its queue.
func (s *queuedServer) Acknowledge(ctx context.Context, id *queuerpc.ID) (*emptypb.Empty, error) {
	if id == nil {
		return &emptypb.Empty{}, errors.New("nil id specification in Acknowledge")
	}
	// Convert the id
	x, err := queuerpc.ToID(id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the underlying driver
	return &emptypb.Empty{}, s.d.Acknowledge(ctx, x)
}

// Requeue requeues the message with the given ID.
func (s *queuedServer) Requeue(ctx context.Context, id *queuerpc.ID) (*emptypb.Empty, error) {
	if id == nil {
		return &emptypb.Empty{}, errors.New("nil id specification in Requeue")
	}
	// Convert the id
	x, err := queuerpc.ToID(id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	// Hand off to the underlying driver
	return &emptypb.Empty{}, s.d.Requeue(ctx, x)
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new queued server.
func NewServer(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &queuedServer{
		d: opts.Driver,
	}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerErrorInterceptor,
			grpclog.UnaryServerInterceptor(m.Log()),
			grpcmetrics.UnaryServerInterceptor(m.Metrics()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			streamServerErrorInterceptor,
			grpclog.StreamServerInterceptor(m.Log()),
			grpcmetrics.StreamServerInterceptor(m.Metrics()),
		)),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	queuerpc.RegisterQueuedServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer:  m,
		SetMetricser: m,
		Server:       s,
	}, nil
}
