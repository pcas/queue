// Queue describes a message queue.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queue

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/queue/driver"
	"bitbucket.org/pcas/queue/errors"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io"
	"sync"
	"time"
)

// Queue represents a queue.
type Queue interface {
	metrics.Metricser
	log.Logger
	io.Closer
	// Get returns the next message from the queue. The message will,
	// eventually, be automatically requeued if it is not acknowledged.
	Get(context.Context) (Message, error)
	// Put places a message with the given content on the queue.
	Put(context.Context, []byte) error
	// Len returns the number of messages in the queue that are not awaiting
	// acknowledgement.
	Len(context.Context) (int, error)
	// Name returns the name of the queue.
	Name() string
}

// queue implements the Queue interface.
type queue struct {
	d        driver.Interface  // The underlying driver
	name     string            // The queue name
	s        closeSet          // The set of open messages
	closef   func()            // The close function
	lg       log.Interface     // The logger
	met      metrics.Interface // The metrics endpoint
	m        sync.RWMutex      // Mutex protecting the following
	isClosed bool              // Are we closed?
}

////////////////////////////////////////////////////////////////////////////////
// Local functions
////////////////////////////////////////////////////////////////////////////////

// newMessage creates a new representation of a message associated with the given queue, using the given data.
func newMessage(q *queue, id driver.ID, content []byte, deadline time.Time) Message {
	// Launch a background go-routine to periodically refresh the message
	doneC, closedC := refreshWorker(q.d, id, deadline)
	// Create the message
	m := &message{
		d:       q.d,
		id:      id,
		content: content,
		doneC:   doneC,
		closedC: closedC,
		lg:      log.PrefixWith(q.Log(), "[id=%s]", id),
		met:     metrics.TagWith(q.Metrics(), metrics.Tags{"ID": id.String()}),
	}
	// Add the message to the set of open messages
	m.closef = q.s.Include(messageCloser{m})
	return m
}

////////////////////////////////////////////////////////////////////////////////
// queue functions
////////////////////////////////////////////////////////////////////////////////

// Name returns the name of the queue.
func (q *queue) Name() string {
	if q == nil {
		return ""
	}
	return q.name
}

// Close closes the queue.
func (q *queue) Close() error {
	// Sanity check
	if q == nil {
		return nil
	}
	err := func() (err error) {
		// Acquire a write lock
		q.m.Lock()
		defer q.m.Unlock()
		// Is there anything to do?
		if !q.isClosed {
			// Recover from any panics in the driver
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in Close: %v", e)
				}
			}()
			// Close the open messages
			q.s.Close()
			// Mark us as closed
			q.isClosed = true
			// Finally call the close function
			if q.closef != nil {
				q.closef()
			}
		}
		return
	}()
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.QueueCloseError.Wrap(err)
	}
	// Provide log data before returning
	if err == nil {
		q.Log().Printf("Connection to queue closed")
	} else {
		q.Log().Printf("Connection to queue closed with error: %v", err)
	}
	return err
}

// Get returns the next message from the queue. The message will, eventually, be automatically requeued if it is not acknowledged.
func (q *queue) Get(ctx context.Context) (Message, error) {
	// Sanity check
	if q == nil || q.d == nil {
		return nil, errors.QueueClosed.New()
	}
	now := time.Now()
	m, err := func() (m Message, err error) {
		// Acquire a read lock
		q.m.RLock()
		defer q.m.RUnlock()
		// Are we closed?
		if q.isClosed {
			err = errors.QueueClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in Get: %v", e)
			}
		}()
		// Hand off to the driver
		var id driver.ID
		var content []byte
		var deadline time.Time
		if id, content, deadline, err = q.d.Get(ctx, q.name); err != nil {
			return
		}
		// Create the message object and return
		m = newMessage(q, id, content, deadline)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToGetMessage.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(q.Metrics(), "Get", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err != nil {
		q.Log().Printf("Get failed after %s: %v", dur, err)
		return nil, err
	}
	q.Log().Printf("Get succeeded in %s", dur)
	return m, nil
}

// Put places a message with the given content on the queue.
func (q *queue) Put(ctx context.Context, content []byte) error {
	// Sanity check
	if q == nil || q.d == nil {
		return errors.QueueClosed.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Aquire a read lock
		q.m.RLock()
		defer q.m.RUnlock()
		// Are we closed?
		if q.isClosed {
			err = errors.QueueClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in Put: %v", e)
			}
		}()
		// Hand off to the driver
		err = q.d.Put(ctx, q.name, content)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToPutMessage.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(q.Metrics(), "Put", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		q.Log().Printf("Put succeeded in %s", dur)
	} else {
		q.Log().Printf("Put failed after %s: %v", dur, err)
	}
	return err
}

// Len returns the number of messages in the queue that are not awaiting acknowledgement.
func (q *queue) Len(ctx context.Context) (int, error) {
	// Sanity check
	if q == nil || q.d == nil {
		return 0, errors.QueueClosed.New()
	}
	now := time.Now()
	n, err := func() (n int, err error) {
		// Aquire a read lock
		q.m.RLock()
		defer q.m.RUnlock()
		// Are we closed?
		if q.isClosed {
			err = errors.QueueClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in Len: %v", e)
			}
		}()
		// Hand off to the driver
		n, err = q.d.Len(ctx, q.name)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToGetQueueLen.Wrap(err)
	}
	// Provide log and metric data before returning
	if err != nil {
		submitPoint(q.Metrics(), "Len", metrics.Fields{
			"Success": false,
		}, dur)
		q.Log().Printf("Len failed after %s: %v", dur, err)
		return 0, err
	}
	submitPoint(q.Metrics(), "Len", metrics.Fields{
		"Success": true,
		"Number":  n,
	}, dur)
	q.Log().Printf("Len succeeded in %s (found %d messages)", dur, n)
	return n, nil
}

// Log returns the log for this queue.
func (q *queue) Log() log.Interface {
	if q == nil || q.lg == nil {
		return log.Discard
	}
	return q.lg
}

// Metrics returns the metrics endpoint for this queue.
func (q *queue) Metrics() metrics.Interface {
	if q == nil || q.met == nil {
		return metrics.Discard
	}
	return q.met
}
