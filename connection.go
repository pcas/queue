// Connection provides a connection to a queuing system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queue

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/queue/driver"
	"bitbucket.org/pcas/queue/errors"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io"
	"sync"
	"time"
)

// Connection provides a connection to a queueing system.
type Connection interface {
	metrics.Metricsable
	log.Logable
	io.Closer
	// Delete deletes the queue with the given name.
	Delete(context.Context, string) error
	// Queue returns the queue with the given name.
	Queue(context.Context, string) (Queue, error)
}

// connection implements the Connection interface.
type connection struct {
	log.BasicLogable
	metrics.BasicMetricsable
	d        driver.Interface // The underlying driver
	s        closeSet         // The set of open queues
	m        sync.RWMutex     // Mutex protecting the following
	isClosed bool             // Are we closed?
	closeErr error            // The error on close (if any)
}

// metricTimeout is the maximum amount of time allowed attempting to submit a metric point.
const metricTimeout = 100 * time.Millisecond

////////////////////////////////////////////////////////////////////////////////
// Local functions
////////////////////////////////////////////////////////////////////////////////

// submitPoint submits to the metrics endpoint m a point with the specified name and fields. The duration d will be automatically added to the fields.
func submitPoint(m metrics.Interface, name string, fields metrics.Fields, d time.Duration) {
	fields["Time"] = d
	pt, err := metrics.NewPoint("queue."+name, fields, nil)
	if err == nil {
		ctx, cancel := context.WithTimeout(context.Background(), metricTimeout)
		defer cancel()
		m.Submit(ctx, pt) // Ignore any error
	}
}

////////////////////////////////////////////////////////////////////////////////
// connection functions
////////////////////////////////////////////////////////////////////////////////

// DriverName returns the name of the associated driver.
func (c *connection) DriverName() string {
	if c == nil || c.d == nil {
		return ""
	}
	return c.d.DriverName()
}

// Close closes the connection.
func (c *connection) Close() error {
	// Sanity check
	if c == nil || c.d == nil {
		return nil
	}
	err := func() (err error) {
		// Acquire a write lock
		c.m.Lock()
		defer c.m.Unlock()
		// Is there anything to do?
		if !c.isClosed {
			// Recover from any panics in the driver
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic in Close: %v", e)
				}
			}()
			// Close the open queues
			c.closeErr = c.s.Close()
			// Close the driver (if necessary)
			if cl, ok := c.d.(io.Closer); ok {
				if err := cl.Close(); c.closeErr == nil {
					c.closeErr = err
				}
			}
			// Mark us as closed
			c.isClosed = true
		}
		// Note any errors and return
		err = c.closeErr
		return
	}()
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.ConnectionCloseError.Wrap(err)
	}
	// Provide log data before returning
	if err == nil {
		c.Log().Printf("Connection to %s closed", c.DriverName())
	} else {
		c.Log().Printf("Connection to %s closed with error: %v", c.DriverName(), err)
	}
	return err
}

// Delete deletes the queue with the given name.
func (c *connection) Delete(ctx context.Context, name string) error {
	// Sanity check
	if len(name) == 0 {
		return errors.IllegalQueueName.New()
	} else if c == nil || c.d == nil {
		return errors.ConnectionClosed.New()
	}
	now := time.Now()
	err := func() (err error) {
		// Acquire a read lock
		c.m.RLock()
		defer c.m.RUnlock()
		// Are we closed
		if c.isClosed {
			err = errors.ConnectionClosed.New()
			return
		}
		// Recover from any panics in the driver
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in Delete: %v", e)
			}
		}()
		// Delete the queue
		err = c.d.Delete(ctx, name)
		return
	}()
	dur := time.Since(now)
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToDeleteQueue.Wrap(err)
	}
	// Provide log and metric data before returning
	submitPoint(c.Metrics(), "Delete", metrics.Fields{
		"Success": err == nil,
	}, dur)
	if err == nil {
		c.Log().Printf("Delete queue \"%s\" succeeded in %s", name, dur)
	} else {
		c.Log().Printf("Delete queue \"%s\" failed after %s: %v", name, dur, err)
	}
	return err
}

// Queue returns the queue with the given name.
func (c *connection) Queue(_ context.Context, name string) (Queue, error) {
	// Sanity check
	if len(name) == 0 {
		return nil, errors.IllegalQueueName.New()
	} else if c == nil || c.d == nil {
		return nil, errors.ConnectionClosed.New()
	}
	q, err := func() (Queue, error) {
		// Acquire a read lock
		c.m.RLock()
		defer c.m.RUnlock()
		// Are we closed
		if c.isClosed {
			return nil, errors.ConnectionClosed.New()
		}
		// Create the queue object
		q := &queue{
			d:    c.d,
			name: name,
			lg:   log.PrefixWith(c.Log(), "[queue=%s]", name),
			met:  metrics.TagWith(c.Metrics(), metrics.Tags{"Queue": name}),
		}
		// Add the queue to the set of open queues
		q.closef = c.s.Include(q)
		return q, nil
	}()
	// Check the error type
	if errors.ShouldWrap(err) {
		err = errors.UnableToConnectToQueue.Wrap(err)
	}
	// Provide log data before returning
	if err != nil {
		c.Log().Printf("Connection to queue \"%s\" failed: %v", name, err)
		return nil, err
	}
	c.Log().Printf("Connected to queue \"%s\"", name)
	return q, nil
}

// SetMetrics sets a metrics endpoint.
func (c *connection) SetMetrics(m metrics.Interface) {
	c.BasicMetricsable.SetMetrics(metrics.TagWith(m, metrics.Tags{
		"DriverName": c.DriverName(),
	}))
}

// New returns a connection wrapping the given driver.
func NewConnection(d driver.Interface) Connection {
	return &connection{
		d: d,
	}
}
